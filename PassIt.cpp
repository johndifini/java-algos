#include <assert.h>
#include <iostream>
using namespace std;

/**
 * Many argue that Java is ONLY pass-by-value,
 * but I'd argue that it's much more nuanced than that.
 * Compare this C++ code with its companion, PassIt.java.
 *
 * run "g++ PassIt.cpp; ./a.out"
 */
class PassIt {
   public:
      int i; // keeping it public for simplicity
      PassIt(int val) {
         i = val;
      }

      // passes a COPY of the object
      static void passByCopy(PassIt obj) {
         obj.i = 22;  // only a "local" change
      }

      // passes a pointer
      static void passByPointer(PassIt* ptr) {
         ptr->i = 33;
         ptr = 0; // better to use nullptr instead if '0'
      }

      // passes an alias (aka reference)
      static void passByAlias(PassIt& ref) {
         ref.i = 44;
      }

      // This is an old-school way of doing it.
      // Check out std::swap for the best way to do this
      static void swap(PassIt** pptr1, PassIt** pptr2) {
         PassIt* tmp = *pptr1;
         *pptr1 = *pptr2;
         *pptr2 = tmp;
      }

      static void testIt() {
         PassIt passIt(11);
         PassIt *pointer = &passIt; // pointer = address of passIt

         // WARN: Could be an expensive copy opperation for
         //       an object containing a lot of data!
         PassIt::passByCopy(passIt);
         assert(passIt.i == 11); // value did NOT change

         PassIt::passByPointer(pointer);
         assert(passIt.i == 33);  // value changed
         assert(pointer == &passIt); // address to which "pointer" points did NOT change

         // WARN: Poor Design
         // The following call does not give any indication that
         // the value of passIt will be changed
         PassIt::passByAlias(passIt);
         assert(passIt.i == 44); // value changed

         PassIt passIt1(1);
         PassIt passIt2(2);
         PassIt* ptr1 = &passIt1;
         PassIt* ptr2 = &passIt2;
         PassIt::swap(&ptr1, &ptr2);
         assert(ptr1->i == 2); // value changed
         assert(ptr2->i == 1); // value changed
         assert(ptr1 == &passIt2); // address changed
         assert(ptr2 == &passIt1); // address changed
      }
};

int main() {
    PassIt::testIt();
}
