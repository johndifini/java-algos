/**
 * Many argue that Java is ONLY pass-by-value,
 * but I'd argue that it's much more nuanced than that.
 * Compare this Java code with its companion, PassIt.cpp.
 *
 * run "javac PassIt.java; java -ea PassIt"
 */
class PassIt {
   public int i; // keeping it public for simplicity
   public PassIt(int val) {
      i = val;
   }

   // There are issues with the Cloneable interface
   // http://books.google.com/books?id=ka2VUBqHiWkC&pg=PA55&lpg=PA55&dq=effective+java+clone&source=bl&ots=yXGhLnv4O4&sig=zvEip5tp5KGgwqO1sCWgtGyJ1Ns&hl=en&ei=CYANSqygK8jktgfM-JGcCA&sa=X&oi=book_result&ct=result&resnum=3#PPA54,M1
   public PassIt(PassIt toCopy) {
      copy(toCopy);
   }
   public void copy(PassIt toCopy) {
      i = toCopy.i;
   }

   // passes a copy of the variable
   // NOTE: in java only primitives are pass-by-copy
   public static void passByCopy(int copy) {
      copy = 33;  // only a "local" change
   }

   // No such thing as pointers in Java
   /*
   public static void passByPointer(PassIt *ptr) {
      ptr->i = 33;
      ptr = 0; // better to use nullptr instead if '0'
   }
   */

   // passes an alias (aka reference)
   public static void passByAlias(PassIt ref) {
      ref.i = 44;
   }

   // passes aliases (aka references),
   // but need to do "manual", potentially expensive copies
   public static void swap(PassIt ref1, PassIt ref2) {
      PassIt tmp = new PassIt(ref1);
      ref1.copy(ref2);
      ref2.copy(tmp);
   }

   static void testIt() {
      int j = 22;
      passByCopy(j);
      // NOTE: run java -ae for "assert" to work
      assert j == 22; // value did NOT change

      //passByPointer(pointer); - no such think in Java

      PassIt passIt = new PassIt(11);
      passByAlias(passIt);
      assert passIt.i == 44; // value changed        

      PassIt passIt1 = new PassIt(1);
      PassIt passIt2 = new PassIt(2);
      // WARN: Could be an expensive copy opperation for
      //       an object containing a lot of data!
      swap(passIt1, passIt2);
      assert passIt1.i == 2; // value changed
      assert passIt2.i == 1; // value changed
   }

   public static void main(String... args) {
      testIt();
   }
}
