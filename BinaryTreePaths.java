import java.util.*;


/**
 * Problem - http://www.lintcode.com/en/problem/binary-tree-paths/
 * Given a binary tree, return all root-to-leaf paths.
 *
 * Example
 * Given the following binary tree:
 *     1
 *   /   \
 *  2     3
 *   \
 *    5
 * All root-to-leaf paths are:
 *  [
 *    "1->2->5",
 *    "1->3"
 *  ]
 * 
 * @see https://discuss.leetcode.com/topic/40726/c-non-recursive-version-and-recursive-version
 * @see https://en.wikipedia.org/wiki/Tree_traversal#Pre-order_2
 */
public class BinaryTreePaths {
   /**
    * This solution uses an iterative (non-recursive) Pre-order traversal per the following pseudocode:
    * iterativePreorder(node)
    *   if (node = null)
    *     return
    *   s ← empty stack
    *   s.push(node)
    *   while (not s.isEmpty())
    *     node ← s.pop()
    *     visit(node)
    *     //right child is pushed first so that left is processed first
    *     if (node.right ≠ null)
    *       s.push(node.right)
    *     if (node.left ≠ null)
    *       s.push(node.left)
    * 
    * @param root the root of the binary tree
    * @return all root-to-leaf paths
    */
   public List<String> binaryTreePaths(TreeNode root) {
      List<String> result = new ArrayList<String>();


      if (root == null)
         return result;


      Deque<TreeNode> traversalDeque = new ArrayDeque<TreeNode>();
      traversalDeque.addFirst(root);
      Deque<String> pathDeque = new ArrayDeque<String>();
      pathDeque.addFirst(Integer.toString(root.val));
      while (!traversalDeque.isEmpty()) {
         TreeNode node = traversalDeque.removeFirst();
         String path = pathDeque.removeFirst();


         // visit the node
         if (node.left == null && node.right == null) {
            result.add(path);
            // Continue is not as elegant as an else block but
            // I wanted to keep the code to resemble the pre-order traversal pattern
            continue;
         }


         // right child is pushed first so that left is processed first
         if (node.right != null) {
            traversalDeque.addFirst(node.right);
            pathDeque.addFirst(path+"->"+node.right.val);
         }
         
         if (node.left != null) {
            traversalDeque.addFirst(node.left);
            pathDeque.addFirst(path+"->"+node.left.val);
         }
      }


      return result;
   }


   /**
    * Test Cases
    * Don't forget the -enableassertions flag when executing java.
    */
   public static void main(String[] args) {
      System.out.println("Once upon a problem…");


      BinaryTreePaths btp = new BinaryTreePaths();
      List<String> result;
      List<String> expected;


      // 1st Test Case
      //       1
      //     /   \
      //    2     3
      //      \
      //       5
      TreeNode tree1 = new TreeNode(1);
      tree1.left = new TreeNode(2);
      tree1.left.right = new TreeNode(5);
      tree1.right = new TreeNode(3);


      result = btp.binaryTreePaths(tree1);
      String[] expectedArr1 = new String[] {"1->2->5", "1->3"};
      expected = Arrays.asList(expectedArr1);
      System.out.println(result);
      assert result.equals(expected) : "expected<"+expected+"> actual<"+result+">";


      // 2nd Test Case
      //       1
      TreeNode tree2 = new TreeNode(1);


      result = btp.binaryTreePaths(tree2);
      String[] expectedArr2 = new String[] {"1"};
      expected = Arrays.asList(expectedArr2);
      System.out.println(result);
      assert result.equals(expected) : "expected<"+expected+"> actual<"+result+">";


      // 3rd Test Case
      //       1
      //     /   \
      //    2     3
      //         /
      //        5
      //         \
      //          6
      TreeNode tree3 = new TreeNode(1);
      tree3.left = new TreeNode(2);
      tree3.right = new TreeNode(3);
      tree3.right.left = new TreeNode(5);
      tree3.right.left.right = new TreeNode(6);


      result = btp.binaryTreePaths(tree3);
      String[] expectedArr3 = new String[] {"1->2","1->3->5->6"};
      expected = Arrays.asList(expectedArr3);
      System.out.println(result);
      assert result.equals(expected) : "expected<"+expected+"> actual<"+result+">";


      // 4th Test Case
      //       1
      //     /   \
      //    2     3
      //         / \
      //        4   5
      TreeNode tree4 = new TreeNode(1);
      tree4.left = new TreeNode(2);
      tree4.right = new TreeNode(3);
      tree4.right.left = new TreeNode(4);
      tree4.right.right = new TreeNode(5);


      result = btp.binaryTreePaths(tree4);
      String[] expectedArr4 = new String[] {"1->2", "1->3->4", "1->3->5"};
      expected = Arrays.asList(expectedArr4);
      System.out.println(result);
      assert result.equals(expected) : "expected<"+expected+"> actual<"+result+">";
   }


   /**
    * TreeNode definition provided in problem statement
    */
   private static class TreeNode {
      public int val;
      public TreeNode left, right;
      public TreeNode(int val) {
         this.val = val;
         this.left = this.right = null;
      }
   }
}


/*
A Walk Thru an Example:
      1
    /   \
   2     3
     \
      5
INIT:  traversalDeque = [1]; pathDeque = [1]


node traversalDeque path    pathDeque      result
1    [2,3]          1       [1->2,1->3]
2    [5,3]          1->2    [1->2->5,1->3]
5    [3]            1->2->5 [1->3]         [1->2->5]
3    []             1->3    []             [1->2->5,1->3]


      1
    /   \
   2     3
        / \
       4   5
INIT:  traversalDeque = [1]; pathDeque = [1]


node traversalDeque path    pathDeque         result
1    [2,3]          1       [1->2,1->3]
2    [3]            1->2    [1->3]            [1->2]
3    [4,5]          1->3    [1->3->4,1->3->5]
4    [5]            1->3->4 [1->3->5]         [1->2,1->3->4]
5    []             1->3->5 []                [1->2,1->3->4,1->3->5]
*/